#include "JpsiUpsilonTools/JpsiFinder.h"
#include "JpsiUpsilonTools/JpsiPlus1Track.h"
#include "JpsiUpsilonTools/JpsiPlus2Tracks.h"
//#include "JpsiUpsilonTools/JpsiEEFinder.h"
#include "JpsiUpsilonTools/PrimaryVertexRefitter.h"
#include "JpsiUpsilonTools/JpsiAlg.h"
#include "JpsiUpsilonTools/JpsiExample.h"
//#include "JpsiUpsilonTools/JpsiEEAlg.h"
//#include "JpsiUpsilonTools/JpsiEEExample.h"
//#include "JpsiUpsilonTools/JpsiJpsiEEExample.h"

using namespace Analysis;

DECLARE_COMPONENT( JpsiFinder )
DECLARE_COMPONENT( JpsiPlus1Track )
DECLARE_COMPONENT( JpsiPlus2Tracks )
//DECLARE_COMPONENT( JpsiEEFinder )
DECLARE_COMPONENT( PrimaryVertexRefitter )
DECLARE_COMPONENT( JpsiAlg )
//DECLARE_COMPONENT( JpsiEEAlg ) 
DECLARE_COMPONENT( JpsiExample )
//DECLARE_COMPONENT( JpsiEEExample )
//DECLARE_COMPONENT( JpsiJpsiEEExample )

