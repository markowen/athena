#include "DataQualityTools/DataQualityFatherMonTool.h"
//#include "DataQualityTools/DQTMuTrkEff.h"
#include "DataQualityTools/DQTDetSynchMonTool.h"
#include "DataQualityTools/DQTMuonIDTrackTool.h"
//#include "DataQualityTools/DQTElectronQualityTool.h"
//#include "DataQualityTools/DQTCaloClusterTool.h"
//#include "DataQualityTools/DQTNonCollBkg_ZDC.h"
#include "DataQualityTools/DQTBackgroundMon.h"
//#include "DataQualityTools/DQTGlobalWFinderTool.h"
//#include "DataQualityTools/DQTRateMonTool.h"
#include "DataQualityTools/DQTDataFlowMonTool.h"
//#include "DataQualityTools/DQTTopLeptonJetsFinderTool.h"
//#include "DataQualityTools/DQTGlobalTopDilFinderTool.h"
#include "DataQualityTools/DQTGlobalWZFinderTool.h"
#include "DataQualityTools/DQTLumiMonTool.h"

DECLARE_COMPONENT( DataQualityFatherMonTool )
//DECLARE_COMPONENT( DQTMuTrkEff )
DECLARE_COMPONENT( DQTDetSynchMonTool )
DECLARE_COMPONENT( DQTMuonIDTrackTool )
//DECLARE_COMPONENT( DQTElectronQualityTool )
//DECLARE_COMPONENT( DQTGlobalWFinderTool )
//DECLARE_COMPONENT( DQTNonCollBkg_ZDC )
DECLARE_COMPONENT( DQTBackgroundMon )
//DECLARE_COMPONENT( DQTRateMonTool )
//DECLARE_COMPONENT( DQTCaloClusterTool )
DECLARE_COMPONENT( DQTDataFlowMonTool )
//DECLARE_COMPONENT( DQTTopLeptonJetsFinderTool )
//DECLARE_COMPONENT( DQTGlobalTopDilFinderTool )
DECLARE_COMPONENT( DQTGlobalWZFinderTool )
DECLARE_COMPONENT( DQTLumiMonTool )

